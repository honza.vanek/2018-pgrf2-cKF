package transformops;

import rasterdata.RasterImage;
import rasterops.LineRasterizer;
import rasterops.TriangleRasterizer;
import transforms.Mat4;
import transforms.Point3D;
import transforms.Vec2D;
import transforms.Vec3D;

import java.util.List;
import java.util.function.BiFunction;

public class RendererSolid<T> implements Renderer<T> {
    private final TriangleRasterizer<T> triangler;
    private final BiFunction<Point3D, Double, T> shader;

    public RendererSolid(TriangleRasterizer<T> triangler, BiFunction<Point3D, Double, T> shader) {
        this.triangler = triangler;
        this.shader = shader;
    }

    @Override
    public RasterImage<T> render(
            RasterImage<T> background,
            List<Point3D> vertices,
            List<Integer> indices,
            T value, Mat4 mat) {
        RasterImage<T> result = background;
        for (int i = 0; i < indices.size(); i += 3) { //triangle list
            Point3D p1 = vertices.get(indices.get(i));
            Point3D p2 = vertices.get(indices.get(i + 1));
            Point3D p3 = vertices.get(indices.get(i + 2));
            Point3D tp1 = p1.mul(mat);
            Point3D tp2 = p2.mul(mat);
            Point3D tp3 = p3.mul(mat);
            
            if (tp1.getZ() > tp2.getZ()) {
                Point3D aux = tp1;
                tp1 = tp2; tp2 = aux;
                aux = p1;
                p1 = p2; p2 = aux;
            }
            if (tp2.getZ() > tp3.getZ()) {
                Point3D aux = tp3;
                tp3 = tp2; tp2 = aux;
                aux = p3;
                p3 = p2; p2 = aux;
            }
            if (tp1.getZ() > tp2.getZ()) {
                Point3D aux = tp1;
                tp1 = tp2; tp2 = aux;
                aux = p1;
                p1 = p2; p2 = aux;
            }
            result = clip(result, tp1, tp2, tp3, p1, p2, p3);
        }
        return result;
    }

    private RasterImage<T> clip(
            final RasterImage<T> background,
            final Point3D tp1,
            final Point3D tp2,
            final Point3D tp3,
            final Point3D p1,
            final Point3D p2,
            final Point3D p3
    ) {

        if (tp3.getZ() < 0) return background;
        if (tp2.getZ() < 0) {
            final double ta = - tp3.getZ() / (tp2.getZ() - tp3.getZ());
            final Point3D tpa = tp3.mul(1 - ta).add(tp2.mul(ta));
            final Point3D pa = p3.mul(1 - ta).add(p2.mul(ta));
            final double tb = - tp1.getZ() / (tp3.getZ() - tp1.getZ());
            final Point3D tpb = tp1.mul(1 - tb).add(tp3.mul(tb));
            final Point3D pb = p1.mul(1 - tb).add(p3.mul(tb));
            return render(background, tp3, tpa, tpb, p3, pa, pb);
        }
        if (tp1.getZ() < 0) {
            final double ta = -tp2.getZ() / (tp1.getZ() - tp2.getZ());
            final Point3D tpa = tp2.mul(1 - ta).add(tp1.mul(ta));
            final Point3D pa = p2.mul(1 - ta).add(p1.mul(ta));
            final double tb = -tp1.getZ() / (tp3.getZ() - tp1.getZ());
            final Point3D tpb = tp1.mul(1 - tb).add(tp3.mul(tb));
            final Point3D pb = p1.mul(1 - tb).add(p3.mul(tb));
            return render(render(background, tp3, tpa, tpb, p3, pa, pb), tp3, tp2, tpa, p3, p2, pa);
        }

        return render(background, tp1, tp2, tp3, p1, p2, p3);
    }


    private RasterImage<T> render(
            RasterImage<T> background,
            final Point3D tp1,
            final Point3D tp2,
            final Point3D tp3,
            final Point3D p1,
            final Point3D p2,
            final Point3D p3
            ) {
        RasterImage<T> result = background;
        final Vec3D v1 = tp1.dehomog().get();
        final Vec3D v2 = tp2.dehomog().get();
        final Vec3D v3 = tp3.dehomog().get();
        final Vec2D lv1 = v1
                .ignoreZ()
                .add(new Vec2D(1,1))
                .mul(0.5);
        final Vec2D lv2 = v2
                .ignoreZ()
                .add(new Vec2D(1,1))
                .mul(0.5);
        final Vec2D lv3 = v3
                .ignoreZ()
                .add(new Vec2D(1,1))
                .mul(0.5);
        result = triangler.rasterize(result,
                lv1.getX(), lv1.getY(),
                lv2.getX(), lv2.getY(),
                lv3.getX(), lv3.getY(),
                shader.apply(p1, v1.getZ()),
                shader.apply(p2, v2.getZ()),
                shader.apply(p3, v3.getZ())
        );
        return result;
    }


}
