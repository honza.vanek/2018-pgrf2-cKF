package objectdata;

import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;

public class CubeTriangles implements Mesh{
    private final CubeEdges cube;
    private final List<Integer> indices;
    public CubeTriangles(final CubeEdges cube) {
        this.cube = cube;

        indices = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            indices.add(i);
            indices.add((i + 1) % 4);
            indices.add((i + 1) % 4 + 4);

            indices.add(i);
            indices.add(i + 4);
            indices.add((i + 1) % 4 + 4);
        }
        for (int i = 0; i < 5; i += 4) {
            indices.add(i);
            indices.add(i + 1);
            indices.add(i + 2);

            indices.add(i);
            indices.add(i + 2);
            indices.add(i + 3);
        }

    }
    @Override
    public List<Point3D> getVertice() {
        return cube.getVertice();
    }

    @Override
    public List<Integer> getIndices() {
        return indices;
    }
}
