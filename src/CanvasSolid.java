import objectdata.CubeEdges;
import objectdata.CubeTriangles;
import objectdata.Mesh;
import rasterdata.*;
import rasterops.LineRasterizerDDA;
import rasterops.TriangleRasterizer;
import rasterops.TriangleRasterizerScan;
import transformops.Renderer;
import transformops.RendererSolid;
import transformops.RendererWireframe;
import transforms.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Optional;
import java.util.function.Function;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2017
 */


/*
Plan:
X objectdata/CubeTriangles
X rasterops/TriangleRasterizer TODO druha pulka
X rasterdata/DepthPixel
X rasterdata/ImageBlender
X transformops/RendererSolid
 */
public class CanvasSolid {

	private final JFrame frame;
	private final JPanel panel;

	private RasterImage<DepthPixel> rasterImage;
	private final TriangleRasterizer<DepthPixel> triangler;
	private int previousX, previousY;

	private final Mesh cube = new CubeTriangles(new CubeEdges());
	private final Renderer<DepthPixel> render;

	private Camera cam;
	private final Mat4 persp;

	private static final DepthPixel CLEAR_PIXEL = new DepthPixel(new Col(), 1);

	public CanvasSolid(final int width, final int height) {
		frame = new JFrame();

		frame.setLayout(new BorderLayout());
		frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
		frame.setResizable(false);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		rasterImage = new RasterImageBlender<>(new RasterImageImmutable<>(width, height,CLEAR_PIXEL),
			(newPixel, oldPixel) -> newPixel.getDepth() < oldPixel.getDepth() ? newPixel : oldPixel
		);

		triangler = new TriangleRasterizerScan<>(
				DepthPixel::mul, DepthPixel::add
		);

		render = new RendererSolid<>(triangler,
				(vertex, z) -> new DepthPixel(new Col(vertex), z)
		);

		cam = new Camera()
				.withPosition(new Vec3D(5,3,2))
				.withAzimuth(Math.PI)
				.withZenith(-Math.atan2(2, 5));

		persp =  new Mat4PerspRH(Math.PI / 3,
				(double)height / width, 0.1, 1000);

		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));


		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				previousX = e.getX();
				previousY = e.getY();
			}
		});
		panel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				cam = cam
						.addAzimuth((previousX - e.getX()) / (double) width)
						.addZenith((previousY - e.getY()) / (double) width);
				draw();
				panel.repaint();
				previousX = e.getX();
				previousY = e.getY();
			}
		});
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				switch (e.getKeyChar()) {
					case 'w': cam = cam.forward(0.3); break;
					case 's': cam = cam.backward(0.3); break;
					case 'a': cam = cam.left(0.3); break;
					case 'd': cam = cam.right(0.3); break;
				}
				draw();
				panel.repaint();
			}
		});

		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);
	}

	public void clear() {
		rasterImage = rasterImage.cleared(CLEAR_PIXEL);
	}

	public void present(final Graphics graphics) {
		new RasterImagePresenterAWT<>(rasterImage,
				pixel -> pixel.getColor().getARGB()
		).present(graphics);
	}

	public void draw() {
		clear();
		rasterImage = render.render(rasterImage, cube.getVertice(), cube.getIndices(), CLEAR_PIXEL, cam.getViewMatrix().mul(persp));
	}

	public void start() {
		draw();
		panel.repaint();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new CanvasSolid(800, 600)::start);
	}

}