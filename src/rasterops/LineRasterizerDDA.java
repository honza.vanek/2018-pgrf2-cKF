package rasterops;

import io.vavr.collection.Stream;
import rasterdata.RasterImage;
import static java.lang.Math.*;

public class LineRasterizerDDA<PixelType> implements LineRasterizer<PixelType> {

    @Override
    public RasterImage<PixelType> rasterizeLine(
            final RasterImage<PixelType> img,
            final double x1, final double y1, final double x2, final double y2,
            PixelType value) {
        final double rx1 = x1 * (img.getWidth() - 1);
        final double ry1 = (1 - y1) * (img.getHeight() - 1);
        final double rx2 = x2 * (img.getWidth() - 1);
        final double ry2 = (1 - y2) * (img.getHeight() - 1);
        final double rdx = rx2 - rx1, rdy = ry2 - ry1;
        final double max = max(abs(rdx), abs(rdy));
        final double dx = rdx / max, dy = rdy / max;
        return Stream.range(0, (int) max  + 1).foldLeft(img,
                (im, i) -> im.withPixel((int)(rx1 + i * dx), (int)(ry1 + i * dy), value)
        );
    }
}
