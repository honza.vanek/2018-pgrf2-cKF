package rasterops;

import rasterdata.RasterImage;

import java.util.function.BiFunction;

public class TriangleRasterizerScan<PixelType> implements TriangleRasterizer<PixelType> {
    private final BiFunction<PixelType, Double, PixelType> mul;
    private final BiFunction<PixelType, PixelType, PixelType> add;

    public TriangleRasterizerScan(
            BiFunction<PixelType, Double, PixelType> mul,
            BiFunction<PixelType, PixelType, PixelType> add) {
        this.mul = mul;
        this.add = add;
    }

    @Override
    public RasterImage<PixelType> rasterize(
            final RasterImage<PixelType> img,
            final double x1, final double y1,
            final double x2, final double y2,
            final double x3, final double y3,
            final PixelType value1,
            final PixelType value2,
            final PixelType value3) {
        if (y1 < y2)
            return rasterize(
                    img, x2, y2, x1, y1, x3, y3,
                    value2, value1, value3);
        if (y2 < y3)
            return rasterize(
                    img, x1, y1, x3, y3, x2, y2,
                    value1, value3, value2);
        final double rx1 = x1 * (img.getWidth() - 1);
        final double ry1 = (1 - y1) * (img.getHeight() - 1);
        final double rx2 = x2 * (img.getWidth() - 1);
        final double ry2 = (1 - y2) * (img.getHeight() - 1);
        final double rx3 = x3 * (img.getWidth() - 1);
        final double ry3 = (1 - y3) * (img.getHeight() - 1);
        RasterImage<PixelType> result = img;
        for (int r = Math.max((int) ry1 + 1, 0);
             r <= Math.min(ry2, img.getHeight() - 1); r++) {
            final double ta = (r - ry1) / (ry2 - ry1);
            double rxa = rx1 * (1 - ta) + rx2 * ta;
            PixelType va =
                    add.apply(
                        mul.apply(value1, 1 - ta),
                        mul.apply(value2, ta)
                    );
            final double tb = (r - ry1) / (ry3 - ry1);
            double rxb = rx1 * (1 - tb) + rx3 * tb;
            PixelType vb =
                    add.apply(
                            mul.apply(value1, 1 - tb),
                            mul.apply(value3, tb)
                    );
            if (rxa > rxb) {
                final double aux1 = rxa;
                rxa = rxb; rxb = aux1;
                final PixelType aux2 = va;
                va = vb; vb = aux2;
            }
            for (int c = Math.max((int) rxa + 1, 0); c <= Math.min(rxb, img.getWidth() - 1); c++) {
                final double t = (c - rxa) / (rxb - rxa);
                final PixelType v =
                        add.apply(
                                mul.apply(va, 1 - t),
                                mul.apply(vb, t)
                        );
                result = result.withPixel(c, r, v);
            }
        }
        /*TODO druha pulka trojuhelniku*/
        return result;
    }
}
