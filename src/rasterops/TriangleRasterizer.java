package rasterops;

import rasterdata.RasterImage;

public interface TriangleRasterizer<PixelType> {
    RasterImage<PixelType> rasterize(
        RasterImage<PixelType> img,
        double x1, double y1,
        double x2, double y2,
        double x3, double y3,
        PixelType value1, PixelType value2, PixelType value3);
}
