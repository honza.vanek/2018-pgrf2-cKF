package rasterdata;

import transforms.Col;

public class DepthPixel {
    private final Col color;
    private final double depth;

    public DepthPixel(Col color, double depth) {
        this.color = color;
        this.depth = depth;
    }

    public Col getColor() {
        return color;
    }

    public double getDepth() {
        return depth;
    }

    public DepthPixel mul(final double t) {
        return new DepthPixel(color.mul(t), depth * t);
    }

    public DepthPixel add(final DepthPixel other) {
        return new DepthPixel(color.add(other.color), depth + other.depth);
    }

}
