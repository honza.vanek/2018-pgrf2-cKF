package rasterdata;

import java.util.Optional;
import java.util.function.BiFunction;

public class RasterImageBlender<PixelType> implements RasterImage<PixelType> {
    private final RasterImage<PixelType> data;
    private final BiFunction<PixelType, PixelType, PixelType> blendFunc;

    public RasterImageBlender(RasterImage<PixelType> data, BiFunction<PixelType, PixelType, PixelType> blendFunc) {
        this.data = data;
        this.blendFunc = blendFunc;
    }

    @Override
    public Optional<PixelType> getPixel(int c, int r) {
        return data.getPixel(c, r);
    }

    @Override
    public RasterImage<PixelType> withPixel(int c, int r, PixelType value) {
        return data.getPixel(c, r).flatMap(
            oldValue -> Optional.of(new RasterImageBlender<>(
                data.withPixel(
                        c, r,blendFunc.apply(value, oldValue)),
                blendFunc))
        ).orElse(this);
    }

    @Override
    public RasterImage<PixelType> cleared(PixelType pixel) {
        return new RasterImageBlender<>(
                data.cleared(pixel), blendFunc);
    }

    @Override
    public int getWidth() {
        return data.getWidth();
    }

    @Override
    public int getHeight() {
        return data.getHeight();
    }
}
